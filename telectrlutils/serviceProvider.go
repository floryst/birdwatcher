package telectrlutils

import (
	"bytes"
	"log"
	"net"
	"strconv"
)

type packetInfo struct {
	addr   net.Addr
	packet []byte
}

// DiscoverableServer server
type DiscoverableServer struct {
	conn        net.PacketConn
	stopChan    chan bool
	errChan     chan error
	packetChan  chan packetInfo
	ServiceName string
	ServicePort int
}

func (sv DiscoverableServer) tryHandlePacket(pkt packetInfo) {
	if len(pkt.packet) < 5 {
		return
	}
	if string(pkt.packet[:5]) != "SRCH:" {
		return
	}
	if string(bytes.Trim(pkt.packet[5:], "\x00")) == sv.ServiceName {
		resp := []byte("SRVC:" + sv.ServiceName + ":" + strconv.Itoa(sv.ServicePort))
		_, err := sv.conn.WriteTo(resp, pkt.addr)
		if err != nil {
			sv.errChan <- err
		}
	}
}

// Start starts discoverable server
func (sv DiscoverableServer) Start() error {
	if sv.conn == nil {
		conn, err := net.ListenPacket("udp4", ":4111")
		if err != nil {
			return err
		}
		sv.conn = conn

		go func() {
			for {
				select {
				case <-sv.stopChan:
					sv.conn.Close()
					sv.conn = nil
				case e := <-sv.errChan:
					log.Printf("Error: %s\n", e)
				case p := <-sv.packetChan:
					sv.tryHandlePacket(p)
				}
			}
		}()

		go func() {
			for {
				if sv.conn == nil {
					return
				}
				recv := make([]byte, 512)
				n, addr, err := sv.conn.ReadFrom(recv)
				if err != nil {
					sv.errChan <- err
				} else if n > 0 {
					pkt := packetInfo{
						addr:   addr,
						packet: recv,
					}
					sv.packetChan <- pkt
				}
			}
		}()
	}

	return nil
}

// IsRunning is server running
func (sv DiscoverableServer) IsRunning() bool {
	return sv.conn != nil
}

// Stop stops the server
func (sv DiscoverableServer) Stop() {
	if sv.conn != nil {
		sv.stopChan <- true
	}
}

// MakeDiscoverableServer make this server
func MakeDiscoverableServer(serviceName string, servicePort int) DiscoverableServer {
	server := DiscoverableServer{
		conn:        nil,
		stopChan:    make(chan bool, 1),
		errChan:     make(chan error, 1),
		packetChan:  make(chan packetInfo, 1),
		ServiceName: serviceName,
		ServicePort: servicePort,
	}
	return server
}
