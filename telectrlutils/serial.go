package telectrlutils

import (
	"bytes"
	"os"

	"syscall"

	"github.com/pkg/term/termios"
	"golang.org/x/sys/unix"
)

// Port a serial port
type Port struct {
	f *os.File
}

// TerminatorFunc terminator
type TerminatorFunc func([]byte) int

// OpenSerialPort opens a serial port
func OpenSerialPort(devName string) (*Port, error) {
	var err error

	fp, err := os.OpenFile(devName, unix.O_RDWR|unix.O_NOCTTY|unix.O_NONBLOCK, 0666)
	if err != nil {
		return nil, err
	}

	defer func() {
		if err != nil && fp != nil {
			fp.Close()
		}
	}()

	tty := syscall.Termios{}
	if err := termios.Tcgetattr(fp.Fd(), &tty); err != nil {
		return nil, err
	}

	tty.Cflag &= ^uint32(unix.PARENB)
	tty.Cflag &= ^uint32(unix.CSTOPB)
	tty.Cflag |= uint32(unix.CS8)
	tty.Cflag &= uint32(unix.CRTSCTS)
	tty.Cflag |= uint32(unix.CREAD | unix.CLOCAL)

	tty.Lflag &= ^uint32(unix.ICANON)
	tty.Lflag &= ^uint32(unix.ECHO | unix.ECHOE | unix.ECHONL)
	tty.Lflag &= ^uint32(unix.ISIG)

	tty.Iflag &= ^uint32(unix.IXON | unix.IXOFF | unix.IXANY)
	tty.Iflag &= ^uint32(unix.IGNBRK | unix.BRKINT | unix.PARMRK | unix.ISTRIP | unix.INLCR | unix.IGNCR | unix.ICRNL)

	tty.Oflag &= ^uint32(unix.OPOST)
	tty.Oflag &= ^uint32(unix.ONLCR)

	tty.Cc[unix.VTIME] = 50 // deciseconds for reply wait-time
	tty.Cc[unix.VMIN] = 0

	// substitute for cfset[io]speed
	tty.Ispeed = unix.B9600
	tty.Ospeed = unix.B9600

	if err := termios.Tcsetattr(fp.Fd(), termios.TCSANOW, &tty); err != nil {
		return nil, err
	}

	return NewPort(fp), nil
}

// NewPort creates a port struct
func NewPort(fp *os.File) *Port {
	return &Port{f: fp}
}

// HashTermFunc tele response terminator func hash-terminated messages
func HashTermFunc(buf []byte) int {
	return bytes.IndexByte(buf, byte('#'))
}

// ZeroOneTermFunc tele response terminator func for 0 or 1
func ZeroOneTermFunc(buf []byte) int {
	b := buf[0]
	if b == '0' || b == '1' {
		return 1
	}
	return -1
}

// ReadTeleResponse wheee
func (p *Port) ReadTeleResponse(termFunc TerminatorFunc) ([]byte, error) {
	outbuf := make([]byte, 256)
	offset := 0
	for {
		n, err := p.Read(outbuf[offset:])
		if err != nil {
			return nil, err
		}
		if n == 0 {
			return outbuf, nil
		}
		mlen := termFunc(outbuf)
		if mlen > -1 {
			return outbuf[:mlen], nil
		}
		offset += n
	}
}

func (p *Port) Read(b []byte) (int, error) {
	return p.f.Read(b)
}

func (p *Port) Write(b []byte) (int, error) {
	return p.f.Write(b)
}

// Close close Port
func (p *Port) Close() {
	if p.f != nil {
		p.f.Close()
		p.f = nil
	}
}
