import sys
import time
import paho.mqtt.client as mqtt

PHONECTRL_CONTROL_TOPIC = 'phonectrl/control'
TELECTRL_CONTROL_TOPIC = 'telectrl/control'
STREAM_CONTROL_TOPIC = 'streamsrv/control'

def on_connect(client, userdata, flags, rc):
    print(f'connected with result code {rc}')

def on_message(client, userdata, msg):
    print('on message:', msg)

def on_publish(client, userdata, mid):
    print('on publish mqtt ID:', mid)

client = mqtt.Client()
client.on_connect = on_connect
client.on_publish = on_publish
client.on_message = on_message

if len(sys.argv) != 3:
    print('control command')
    sys.exit(1)

control = sys.argv[1]
command = sys.argv[2]
topic = ''
if control == 'phone':
    topic = PHONECTRL_CONTROL_TOPIC
elif control == 'tele':
    topic = TELECTRL_CONTROL_TOPIC
elif control == 'stream':
    topic = STREAM_CONTROL_TOPIC
else:
    print('invalid topic')
    sys.exit(1)

try:
    client.connect('localhost', 1883, 60)
    client.loop_start()

    print('topic:', topic, 'command:', command)
    tok = client.publish(topic, command)
    #client.publish(PHONECTRL_CONTROL_TOPIC, 'START_STREAM').wait_for_publish()
    #client.publish(PHONECTRL_CONTROL_TOPIC, 'STOP_STREAM').wait_for_publish()
    #client.publish(PHONECTRL_CONTROL_TOPIC, 'TAKE_PHOTO').wait_for_publish()

    #client.publish(TELECTRL_CONTROL_TOPIC, 'STOP_ALL').wait_for_publish()

    #client.subscribe(TELECTRL_CONTROL_TOPIC)
    #client.loop_forever()
    time.sleep(1)
    tok.wait_for_publish()
    print('done')
except KeyboardInterrupt:
    pass
finally:
    client.disconnect()
    print('Disconnected')
