const path = require('path')
const express = require('express')
const http = require('http')
const MQTT = require('async-mqtt')
const socketIO = require('socket.io')

const common = require('./public/common.js')

const MQTT_BROKER = process.env.MQTT_BROKER || 'tcp://localhost:1883'

function bindMqttAndSocket(mqttc, sock) {
  const controlTopics = [
    common.TELECTRL_CONTROL_TOPIC,
    // common.PHONECTRL_CONTROL_TOPIC,
    // common.STREAM_CONTROL_TOPIC,
  ];

  controlTopics.forEach((topic) => {
    sock.on(topic, async (cmd, respond) => {
      try {
        console.log('publishing:', topic, cmd)
        await mqttc.publish(topic, cmd)
        respond({ ok: true })
      } catch (e) {
        respond({
          ok: false,
          topic: topic,
          reason: e.message || String(e),
        })
      }
    })
  });
}

async function main() {
  const app = express()
  const server = http.createServer(app)
  const io = socketIO(server)

  mqttc = MQTT.connect(MQTT_BROKER)

  app.use(express.json())
  app.use(express.static('public'))

  app.get('/', (req, res) => {
      res.sendFile(path.join(__dirname, 'index.html'))
  })

  io.on('connection', (socket) => {
    bindMqttAndSocket(mqttc, socket)
  })

  console.log('starting')
  server.listen(3000, '0.0.0.0')
}

main()

/*
async function invokeTeleCommand(cmd, mqttc) {
  if (TELE_COMMANDS.include(cmd)) {
    await client.publish(TELECTRL_CONTROL_TOPIC, cmd)
  } else {
    throw new Error('Invalid command')
  }
}

async function moveTeleDir(dir, stop, mqttc) {
  const cmd = (stop ? 'STOP_' : '') + dir
  await invokeTeleCommand(cmd, mqttc)
}

async function setSlewRate(rate, mqttc) {
  const cmd = `SLEW_RATE_${rate}`
  await invokeTeleCommand(cmd, mqttc)
}

function registerHandlers(app, mqttc) {
  app.get('/', (req, res) => {
      res.sendFile(path.join(__dirname, 'index.html'))
  })

  app.post('/move', async (req, res) => {
    const { dir, stop } = req.body
    if (dir && typeof stop !== 'undefined') {
      try {
        await moveTeleDir(dir, stop, mqttc)
        res.json({ ok: true })
      } catch (e) {
        console.error('/move:', e)
        res.json({ ok: false, error: 'Failed to move' })
      }
    }
  })

  app.post('/stopAll', async (req, res) => {
    try {
      await invokeTeleCommand('STOP_ALL', mqttc)
      res.json({ ok: true })
    } catch (e) {
      console.error('/stopAll:', e)
      res.json({ ok: false, error: 'Failed to stop all' })
    }
  })

  app.post('/setSlewRate', async (req, res) => {
    try {
      await setSlewRate(req.body.rate || 0, mqttc)
      res.json({ ok: true })
    } catch (e) {
      console.error('/setSlewRate:', e)
      res.json({ ok: false, error: 'Failed to set slew rate' })
    }
  })

  app.post('/startRecord', async (req, res) => {
    try {
      await invokeStreamCommand('START_RECORD')
      res.json({ ok: true })
    } catch (e) {
      console.error('/startRecord:', e)
      res.json({ ok: false, error: 'Failed to stop recording' })
    }
  })

  app.post('/stopRecord', async (req, res) => {
    try {
      await invokeStreamCommand('STOP_RECORD')
      res.json({ ok: true })
    } catch (e) {
      console.error('/stopRecord:', e)
      res.json({ ok: false, error: 'Failed to stop recording' })
    }
  })

  app.post('/takePhoto', async (req, res) => {
    try {
      await invokeStreamCommand('STOP_RECORD')
      res.json({ ok: true })
    } catch (e) {
      console.error('/stopRecord:', e)
      res.json({ ok: false, error: 'Failed to stop recording' })
    }
  })
}

/*
app.post('/slewRate', async (req, res) => {
    const { rate = 0 } = req.body
    if (rate < 2 || rate > 8) {
        res.json({
            result: 'nope',
        })
    }

    sock.send(new Uint8Array([rate]), PORT, HOST)

    res.json({
        result: 'ok'
    })
})

app.post('/move', async (req, res) => {
    const { stopAll, move, dir = -1 } = req.body
    if (!stopAll && dir === -1) {
        res.json({
            result: 'nope',
        })
    }

    let cmd = 0
    if (move) {
        cmd |= 0x40
    } else {
        cmd |= 0x80
    }
    if (stopAll) {
        cmd = 0xc0
    } else {
        cmd |= dir
    }

    sock.send(new Uint8Array([cmd]), PORT, HOST)

    res.json({
        result: 'ok'
    })
})

app.listen(3000, '0.0.0.0')
*/
