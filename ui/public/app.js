/* globals: m, io, common */

const socket = io()

function emit(channel, arg) {
  return new Promise((resolve, reject) => {
    socket.emit(channel, arg, (resp) => {
      if (resp.ok) {
        resolve()
      } else {
        reject(resp)
      }
    })
  })
}

function invokeTele(cmd) {
  if (common.TELE_COMMANDS.includes(cmd)) {
    return emit(common.TELECTRL_CONTROL_TOPIC, cmd)
  }
  return Promise.resolve()
}

function startMove(move) {
  console.log('starting move:', move)
  return invokeTele(move)
}

function endMove(move) {
  console.log('ending move:', move)
  const stopMove = `STOP_${move}`
  return invokeTele(stopMove)
}

function stopAllMove() {
  console.log('stop all move')
  return invokeTele('STOP_ALL')
}

function sendSlewRate(rate) {
  const cmd = `SLEW_RATE_${rate}`
  return invokeTele(cmd)
}

const App = {
    view() {
        const btns = [
            { r: 1, c: 2, move: 'UP' },
            { r: 2, c: 1, move: 'LEFT' },
            { r: 2, c: 3, move: 'RIGHT' },
            { r: 3, c: 2, move: 'DOWN' },
        ]
        const bels = btns.map(({ r, c, move }) => {
            return m('button', {
                class: 'ctrlBtn',
                style: {
                    gridColumn: `${c}`,
                    gridRow: `${r}`,
                },
                onmousedown: (e) => {
                    startMove(move)
                      .catch((err) => {
                        console.error(err)
                      })
                },
                onmouseup: (e) => {
                    endMove(move)
                      .catch((err) => {
                        console.error(err)
                      })
                },
                ontouchstart: (e) => {
                    startMove(move)
                      .catch((err) => {
                        console.error(err)
                      })
                },
                ontouchend: (e) => {
                    endMove(move)
                      .catch((err) => {
                        console.error(err)
                      })
                },
            })
        })
        const slewRates = [2, 3, 4]
        const rateBtns = slewRates.map((rate) => {
            return m('button', {
                class: 'ctrlBtn',
                onclick: () => {
                    sendSlewRate(rate).catch(console.error)
                },
            }, [String(rate)])
        })
        return m('div', { class: 'subroot' }, [
            m('div', { class: 'controlsWrapper' }, bels),
            m('div', { class: 'slewRates' }, rateBtns),
            m('button', { class: 'stopAllBtn', onclick: () => stopAllMove() }, ['Stop all movement']),
        ])
    }
}

m.mount(document.getElementById('root'), App)
