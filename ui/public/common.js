(function() {
const common = {
  TELECTRL_CONTROL_TOPIC: 'telectrl/control',
  TELE_COMMANDS: [
    'UP', 'DOWN', 'LEFT', 'RIGHT',
    'STOP_UP', 'STOP_DOWN', 'STOP_LEFT', 'STOP_RIGHT',
    'STOP_ALL',
    'SLEW_RATE_1', 'SLEW_RATE_2', 'SLEW_RATE_3', 'SLEW_RATE_4',
  ],
};

if (typeof module !== 'undefined') {
  module.exports = common
}

if (typeof window !== 'undefined') {
  window.common = common
}
})()
