import os
import time
import gi
import threading
from os import path
gi.require_version('Gst', '1.0')
gi.require_version('GstBase', '1.0')

from gi.repository import Gst, GObject, GstBase, GLib

def wait_for_msg(bus, *types):
    while True:
        msg = bus.poll(Gst.MessageType.ANY, int(100*1e6))
        if msg is None:
            continue
        if msg.type in types:
            break

def block_probe_cb(pad, info, *user_data):
    print(f'{pad.get_name()} blocked!')
    return Gst.PadProbeReturn.OK

class PreRecordPipeline(object):
    '''
    vtee.
    ! queue name=vrecq
    ! mp4mux name=mux
    ! filesink async=false name=filesink
    '''

    pbin = None
    vidq = None
    audioq = None
    mux = None
    filesink = None

    vidq_src = None
    audioq_src = None
    vidq_src_probe = None
    audioq_src_probe = None
    vbuffer_count = 0
    abuffer_count = 0

    savedir = '.'
    is_video_recording_ready = False

    def __init__(self, savedir='.'):
        self.savedir = savedir

        pbin = Gst.ElementFactory.make('pipeline', 'prerecord-pipeline')
        pvidq = Gst.ElementFactory.make('queue', 'prerecord-video-queue')
        paudioq = Gst.ElementFactory.make('queue', 'prerecord-audio-queue')
        pmux = Gst.ElementFactory.make('mp4mux', 'prerecord-mp4-muxer')
        pfilesink = Gst.ElementFactory.make('filesink', 'prerecord-sink')

        self.pbin = pbin
        self.vidq = pvidq
        self.audioq = paudioq
        self.mux = pmux
        self.filesink = pfilesink

        pbin.add(pvidq)
        pbin.add(paudioq)
        pbin.add(pmux)
        pbin.add(pfilesink)

        pbin.set_property('message-forward', True)
        pvidq.set_properties(
            max_size_time=20 * Gst.SECOND,
            max_size_bytes=0, # disable
            max_size_buffers=0, # disable
            leaky=2, # leaky on downstream, so toss old buffers
        )
        paudioq.set_properties(
            max_size_time=20 * Gst.SECOND,
            max_size_bytes=0, # disable
            max_size_buffers=0, # disable
            leaky=2, # leaky on downstream, so toss old buffers
        )
        pfilesink.set_property('async', False)

        # construct pipeline
        pmux.link(pfilesink)
        mux_video_sink = pmux.get_request_pad('video_%u')
        mux_audio_sink = pmux.get_request_pad('audio_%u')
        pvidq.link_pads('src', pmux, mux_video_sink.get_name()) # vidq -> mux:video_%u
        paudioq.link_pads('src', pmux, mux_audio_sink.get_name()) # audioq -> mux:audio_%u

        self.vidq_src = self.vidq.get_static_pad('src')
        self.vidq_src_probe = self.vidq_src.add_probe(
            Gst.PadProbeType.BLOCK | Gst.PadProbeType.BUFFER,
            block_probe_cb,
            None,
            None,
        )
        self.audioq_src = self.audioq.get_static_pad('src')
        self.audioq_src_probe = self.audioq_src.add_probe(
            Gst.PadProbeType.BLOCK | Gst.PadProbeType.BUFFER,
            block_probe_cb,
            None,
            None,
        )

    @property
    def videoqueue(self):
        return self.vidq

    @property
    def audioqueue(self):
        return self.audioq

    @property
    def videoqueue_src(self):
        return self.vidq_src

    @property
    def audioqueue_src(self):
        return self.audioq_src

    def update_filesink_location(self):
        name = path.join(self.savedir, time.strftime('%Y-%m-%dT%H-%M-%SZ%z.mp4'))
        self.filesink.set_property('location', name)

    def start_recording(self):
        # this is updated in the probe callback below
        self.is_video_recording_ready = False

        self.vbuffer_count = 0
        self.abuffer_count = 0
        self.vidq_src.add_probe(Gst.PadProbeType.BUFFER, self.probe_drop_one_cb, 'video', None)
        self.audioq_src.add_probe(Gst.PadProbeType.BUFFER, self.probe_drop_one_cb, 'audio', None)

        self.vidq_src.remove_probe(self.vidq_src_probe)
        self.audioq_src.remove_probe(self.audioq_src_probe)
        self.vidq_src_probe = None
        self.audoiq_src_probe = None

    def stop_recording(self):
        self.is_video_recording_ready = False
        self.vidq_src_probe = self.vidq_src.add_probe(
            Gst.PadProbeType.BLOCK | Gst.PadProbeType.BUFFER,
            block_probe_cb,
            None,
            None,
        )
        self.audioq_src_probe = self.audioq_src.add_probe(
            Gst.PadProbeType.BLOCK | Gst.PadProbeType.BUFFER,
            block_probe_cb,
            None,
            None,
        )

    def reset_recording(self):
        # clean up muxer and filesink
        self.muxer.set_state(Gst.State.NULL)
        self.filesink.set_state(Gst.State.NULL)

        # generate new save location
        self.update_filesink_location()

        # restart mux and filesink
        # restarting filesink will make it use new location
        self.muxer.set_state(Gst.State.PLAYING)
        self.filesink.set_state(Gst.State.PLAYING)

    def probe_drop_one_cb(self, pad, info, user_data, destroy_data):
        # what pad is this probe installed on (video or audio queue)
        pad_name = user_data
        buf = info.get_buffer()
        if pad_name == 'video':
            if self.vbuffer_count == 0:
                self.vbuffer_count += 1
                print(f'Dropping one video buffer with ts {Gst.TIME_ARGS(buf.pts)}')
                return Gst.PadProbeReturn.DROP
        elif pad_name == 'audio':
            if self.abuffer_count == 0:
                self.abuffer_count += 1
                print(f'Dropping one audio buffer with ts {Gst.TIME_ARGS(buf.pts)}')
                return Gst.PadProbeReturn.DROP

        if pad_name == 'video':
            is_keyframe = not buf.has_flags(Gst.BufferFlags.DELTA_UNIT)
            if is_keyframe:
                # buf is a keyframe, so let buffer through and remove this probe
                print('Keyframe at:', Gst.TIME_ARGS(buf.pts))
                # we now can actually record both video and audio, since
                # we have reached a video keyframe
                self.is_video_recording_ready = True
                return Gst.PadProbeReturn.REMOVE
            # wait for a keyframe
            return Gst.PadProbeReturn.DROP
        elif pad_name == 'audio':
            # wait for video to start recording
            if self.is_video_recording_ready:
                return Gst.PadProbeReturn.REMOVE
            return Gst.PadProbeReturn.DROP

def bus_watch_cb(bus, msg, app):
    t = msg.type
    if Gst.MessageType.ERROR == t:
        err, debug = msg.parse_error()
        print(f'Error: {err}; {debug}')
        return False # remove this bus cb

    elif Gst.MessageType.WARNING == t:
        warn, debug = msg.parse_warning()
        print(f'Warning: {warn}; {debug}')

    elif Gst.MessageType.ELEMENT == t:
        if msg.has_name('GstBinForwarded'):
            # key-value metadata for message
            msg_struct = msg.get_structure()
            # the forwarded message is in the structure under key "message"
            # see GstBin message-forward property for more info
            fwdmsg = msg_struct.get_value('message')
            t = fwdmsg.type

            if Gst.MessageType.EOS == t:
                if app.eos_count == 2:
                    app.eos_count = 0
                    app.prerecord.reset_recording()
                else:
                    app.eos_count += 1
 
    return True


def push_eos(app):
    app.eos_count = 0
    app.pipeline.set_property('message-forward', True)

    peer = app.prerecord.videoqueue_src.get_peer()
    print('pushing eos to pad', peer.get_name())
    peer.send_event(Gst.Event.new_eos())

    # need to send EOS on both pads to properly finish writing both audio and video
    peer = app.prerecord.audioqueue_src.get_peer()
    print('pushing eos to pad', peer.get_name())
    peer.send_event(Gst.Event.new_eos())

    return None

def stop_recording_cb(app):
    print('stop recording')
    app.prerecord.stop_recording()
    GLib.idle_add(push_eos, app)
    return False

def start_recording_cb(app):
    print('starting recording')
    app.prerecord.start_recording()
    GLib.timeout_add_seconds(5, stop_recording_cb, app)
    return False

class App(object):
    pipeline = None
    prerecord = None
    eos_count = 0

if __name__ == '__main__':
    Gst.init(None)
    
    # need to reduce key-int-max to ensure we have sufficient keyframes
    # this will of course increase file size
    pipeline = Gst.parse_launch('''
    videotestsrc is-live=true do-timestamp=true
    ! videorate
    ! video/x-raw,width=640,height=480,format=I420,framerate=25/1
    ! clockoverlay
    ! x264enc tune=zerolatency bitrate=8000 key-int-max=5
    ! tee name=vtee

    audiotestsrc is-live=true do-timestamp=true
    ! audiorate
    ! audio/x-raw
    ! avenc_aac
    ! tee name=atee

    vtee.
    ! queue
    ! avdec_h264
    ! videoconvert
    ! videoscale
    ! autovideosink

    atee.
    ! queue
    ! avdec_aac
    ! audioconvert
    ! audioresample
    ! autoaudiosink
    ''')

    prerecord = PreRecordPipeline(savedir='/dev/shm')
    # required call before using prerecord
    prerecord.update_filesink_location()

    app = App()
    app.pipeline = pipeline
    app.prerecord = prerecord

    # link app and prerecord
    app.pipeline.add(prerecord.pbin)
    vtee = app.pipeline.get_by_name('vtee')
    vtee.link(prerecord.videoqueue)
    atee = app.pipeline.get_by_name('atee')
    atee.link(prerecord.audioqueue)

    app.pipeline.set_state(Gst.State.PLAYING)

    loop = GLib.MainLoop()
    app.pipeline.get_bus().add_watch(GLib.PRIORITY_DEFAULT, bus_watch_cb, app)

    #with open('test.dot', 'w') as fp:
    #    fp.write(Gst.debug_bin_to_dot_data(app.pipeline, Gst.DebugGraphDetails.ALL))

    GLib.timeout_add_seconds(5, start_recording_cb, app)
    loop.run()

    app.pipeline.set_state(Gst.State.NULL)
    Gst.Object.unref(app.pipeline)
