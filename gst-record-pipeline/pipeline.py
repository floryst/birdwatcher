import time
import gi
gi.require_version('Gst', '1.0')
gi.require_version('GstBase', '1.0')

from gi.repository import Gst, GObject, GstBase

Gst.init(None)

def wait_for_msg(bus, types):
    while True:
        msg = bus.poll(Gst.MessageType.ANY, int(100*1e6))
        if msg is None:
            continue
        if msg.type in types:
            break

pipeline = Gst.parse_launch('''
rtspsrc protocols=tcp latency=0 location=rtsp://192.168.0.182:1935
! tee name=t
! rtpjitterbuffer latency=200
! rtph264depay
! h264parse
! queue
! mp4mux
! filesink location=/dev/shm/hi.mp4
t.
! rtpjitterbuffer latency=10
! rtph264depay
! h264parse
! decodebin
! xvimagesink
''')

pl_it = pipeline.iterate_elements()
while True:
    res, el = pl_it.next()
    if el is None:
        break
    if el.name == 'parse':
        # workaround camera PTS issue
        GstBase.BaseParse.set_infer_ts(e, True)
        GstBase.BaseParse.set_pts_interpolation(e, True)
        pass

pipeline.set_state(Gst.State.PLAYING)
bus = pipeline.get_bus()

try:
    start = time.time()
    while time.time()  - start < 20:
        msg = bus.poll(Gst.MessageType.ANY, int(100*1e6))
        if msg is None:
            continue
        t = msg.type
        if t == Gst.MessageType.ERROR:
            err, debug = msg.parse_error()
            print(f'Error: {err}; {debug}')
            break
        elif t == Gst.MessageType.WARNING:
            warn, debug = msg.parse_warning()
            print(f'Error: {warn}; {debug}')
    pipeline.send_event(Gst.Event.new_eos())
    wait_for_msg(bus, [Gst.MessageType.EOS])
except KeyboardInterrupt:
    pipeline.send_event(Gst.Event.new_eos())
    wait_for_msg(bus, [Gst.MessageType.EOS])
finally:
    pipeline.set_state(Gst.State.NULL)
