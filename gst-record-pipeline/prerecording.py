import os
import time
import gi
import threading
from os import path
gi.require_version('Gst', '1.0')
gi.require_version('GstBase', '1.0')

from gi.repository import Gst, GObject, GstBase, GLib

def wait_for_msg(bus, *types):
    while True:
        msg = bus.poll(Gst.MessageType.ANY, int(100*1e6))
        if msg is None:
            continue
        if msg.type in types:
            break

# https://people.freedesktop.org/~tpm/code/test-backlog-recording-h264.c
class AppPipeline(object):
    def __init__(self, savedir='.'):
        self.savedir = savedir
        self.loop = GLib.MainLoop()

        self.pipeline = Gst.parse_launch('''
        videotestsrc
        ! video/x-raw,height=1920,width=1080
        ! clockoverlay
        ! x264enc tune=zerolatency bitrate=8000
        ! tee name=vtee

        vtee.
        ! avdec_h264
        ! videoconvert
        ! autovideosink

        vtee.
        ! queue name=vrecq
        ! mp4mux name=mux
        ! filesink async=false name=filesink
        ''')

        # Forward all children messages, even those that would normally be filtered by the bin.
        self.pipeline.set_property('message-forward', True)

        self.gbus = self.pipeline.get_bus()

        # workaround camera PTS issue
        pl_it = self.pipeline.iterate_elements()
        while True:
            res, el = pl_it.next()
            if el is None:
                break
            if el.name == 'parse':
                # workaround camera PTS issue
                GstBase.BaseParse.set_infer_ts(e, True)
                GstBase.BaseParse.set_pts_interpolation(e, True)
                pass

        self.vrecq = self.pipeline.get_by_name('vrecq')
        self.vrecq.set_properties(
            max_size_time=3 * Gst.SECOND,
            max_size_bytes=0, # disable
            max_size_buffers=0, # disable
            leaky=2, # leaky on downstream, so toss old buffers
        )
        self.vrecq_src = self.vrecq.get_static_pad('src')
        self.vrecq_src_blocking_probe = self.add_blocking_probe(self.vrecq_src)

        self.filesink = self.pipeline.get_by_name('filesink')
        self.mux = self.pipeline.get_by_name('mux')

        # set filesink save file
        self.gensavename()

    def add_blocking_probe(self, pad):
        return pad.add_probe(
            Gst.PadProbeType.BLOCK | Gst.PadProbeType.BUFFER,
            self.queue_block_cb,
            None,
            None,
        )

    def gensavename(self):
        self.cursavename = time.strftime('%Y-%m-%dT%H-%M-%SZ%z.mp4')
        self.filesink.set_property('location',
                path.join(self.savedir, self.cursavename))

    def queue_block_cb(self, pad, info, user_data, destroy_data):
        # block src pad until recording begins
        return Gst.PadProbeReturn.OK

    def prepare_record_cb(self, pad, info, user_data, destroy_data):
        buf = info.get_buffer()
        if self.drop_stuck_buffer:
            self.drop_stuck_buffer = False
            print(f'Dropping one buffer with ts {Gst.TIME_ARGS(buf.pts)}')
            return Gst.PadProbeReturn.DROP

        if not buf.has_flags(Gst.BufferFlags.DELTA_UNIT):
            # buf is a keyframe, so let buffer through and remove this probe
            print('Keyframe at:', Gst.TIME_ARGS(buf.pts))
            print('Recording to:', self.cursavename)
            return Gst.PadProbeReturn.REMOVE
        # wait for a keyframe
        return Gst.PadProbeReturn.DROP

    # if this func ever return False, the watch will be removed
    def bus_watch_cb(self, bus, message, *user_data):
        t = message.type
        if Gst.MessageType.ERROR == t:
            err, debug = message.parse_error()
            print(f'Error: {err}; {debug}')
            self.terminate()
            return False # remove this bus cb

        elif Gst.MessageType.WARNING == t:
            warn, debug = message.parse_warning()
            print(f'Warning: {warn}; {debug}')

        elif message.has_name('GstBinForwarded'):
            # key-value metadata for message
            msg_struct = message.get_structure()
            # the forwarded message is in the structure under key "message"
            # see GstBin message-forwarding property for more info
            fwdmsg = msg_struct.get_value('message')
            t = fwdmsg.type

            if Gst.MessageType.EOS == t:
                # clean up muxer and filesink
                self.mux.set_state(Gst.State.NULL)
                self.filesink.set_state(Gst.State.NULL)
                
                # generate new save location
                self.gensavename()

                # restart mux and filesink
                # restarting filesink will make it use new location
                self.mux.set_state(Gst.State.PLAYING)
                self.filesink.set_state(Gst.State.PLAYING)
        return True
 
    def start_recording(self):
        if self.vrecq_src_blocking_probe is not None:
            print('Start recording')
            
            # use another probe to drop the buffer that's stuck in the blocking probe
            self.drop_stuck_buffer = True
            self.vrecq_src.add_probe(
                Gst.PadProbeType.BUFFER,
                self.prepare_record_cb,
                None,
                None,
            )
            # remove old blocking probe
            self.vrecq_src.remove_probe(self.vrecq_src_blocking_probe)
            self.vrecq_src_blocking_probe = None


    def stop_recording(self):
        # ideally the prepare_record_cb probe is already gone
        # when stop_recording is invoked
        if self.vrecq_src_blocking_probe is None:
            print('Stopping recording')

            # re-add blocking probe
            self.vrecq_src_blocking_probe = self.add_blocking_probe(self.vrecq_src)

            # need to send EOS to muxer source so we can clean up the file
            # EOS to filesink will also clean up and update location property
            # this event should reach source pads, as sending the event on sink pads won't do us
            # much good.
            def send_eos():
                peer = self.vrecq_src.get_peer()
                peer.send_event(Gst.Event.new_eos())

            GLib.idle_add(send_eos)

    def start(self):
        self.pipeline.set_state(Gst.State.PLAYING)
        self.gbus.add_watch(GLib.PRIORITY_DEFAULT, self.bus_watch_cb, None)
        self.loop.run()

    def terminate(self):
        self.loop.quit()

        # remove our bus watch, otherwise we get an error
        self.gbus.remove_watch()

        # clean up muxer and filesink, regardless of recording state
        peer = self.vrecq_src.get_peer()
        peer.send_event(Gst.Event.new_eos())

        # finally, need to tell entire pipeline of EOS
        self.pipeline.send_event(Gst.Event.new_eos())
        wait_for_msg(self.gbus, Gst.MessageType.EOS)

        self.pipeline.set_state(Gst.State.NULL)


def trigger_recording_cb(app):
    app.start_recording()
    # queue stop callback
    GLib.timeout_add_seconds(3, stop_recording_cb, app)
    return False # don't keep callback

def stop_recording_cb(app):
    app.stop_recording()
    GLib.timeout_add_seconds(3, trigger_recording_cb, app)
    return False # don't keep callback

if __name__ == '__main__':
    Gst.init(None)

    app = AppPipeline(savedir='/dev/shm/')
    try:
        GLib.timeout_add_seconds(1, trigger_recording_cb, app)
        app.start()
    except KeyboardInterrupt:
        pass
    finally:
        app.terminate()
