import os
import sys
import time

import gi
gi.require_version('Gst', '1.0')
gi.require_version('GstBase', '1.0')
# need to install gst-rtsp-server from AUR
gi.require_version('GstRtspServer', '1.0')
from gi.repository import Gst, GObject, GstBase, GLib, GstRtspServer

class RtspServer(object):
    def __init__(self):
        # setup rtsp server
        self.rtsp_server = GstRtspServer.RTSPServer.new()
        self.rtsp_server.set_property('service', '1880')

        # mounts is a mapping from uris to media factories
        mounts = self.rtsp_server.get_mount_points()

        # construct a recording media factory
        self.rec_factory = GstRtspServer.RTSPMediaFactory.new()
        self.rec_factory.set_transport_mode(GstRtspServer.RTSPTransportMode.RECORD)
        self.rec_factory.set_eos_shutdown(True) # send EOS to media upon shutdown
        self.rec_factory.set_latency(10)
        self.rec_factory.set_launch('( rtph264depay name=depay0 ! queue ! h264parse ! avdec_h264 ! videoconvert ! autovideosink sync=true rtpmp4gdepay name=depay1 ! queue ! avdec_aac ! audioconvert ! autoaudiosink sync=true )')

        # mount media factory to a uri
        mounts.add_factory('/phone', self.rec_factory)

        # attach server to default main context
        self.rtsp_server.attach(None)

if __name__ == '__main__':
    Gst.init(None)

    server = RtspServer()
    loop = GLib.MainLoop()
    try:
        print('running')
        loop.run()
    except KeyboardInterrupt:
        pass
