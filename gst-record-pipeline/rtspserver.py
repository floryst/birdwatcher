import os
import sys
import time
import threading
import random
from os import path

import paho.mqtt.client as mqtt

import gi
gi.require_version('Gst', '1.0')
gi.require_version('GstBase', '1.0')
# need to install gst-rtsp-server from AUR
gi.require_version('GstRtspServer', '1.0')
from gi.repository import Gst, GObject, GstBase, GLib, GstRtspServer

STREAM_SERVER_CONTROL_TOPIC = 'streamsrv/control'
STREAM_SERVER_STATE_TOPIC = 'streamsrv/state'
PHONECTRL_PHOTO_TOPIC = 'phonectrl/photos'

def block_probe_cb(pad, info, *user_data):
    print(f'{pad.get_name()} blocked!')
    return Gst.PadProbeReturn.OK

def wait_for_msg(bus, *types):
    while True:
        msg = bus.poll(Gst.MessageType.ANY, int(100*1e6))
        if msg is None:
            continue
        if msg.type in types:
            break

class RecordingFactory(GstRtspServer.RTSPMediaFactory):
    def do_create_element(self, url):
        print('do create element')
        # this is the same pipeline as in set_launch()
        # I have it here just to test to ensure do_create_element works
        #pipeline = Gst.parse_launch('( rtph264depay name=depay0 ! queue ! h264parse ! avdec_h264 ! videoconvert ! autovideosink sync=true rtpmp4gdepay name=depay1 ! queue ! avdec_aac ! audioconvert ! autoaudiosink sync=true )')
        #return pipeline

        #pipeline = Gst.parse_launch('''(
        #    rtph264depay name=depay0 ! queue ! h264parse ! avdec_h264 ! queue ! videoconvert ! tee name=rawvideotee
        #    rtpmp4gdepay name=depay1 ! queue ! avdec_aac ! queue ! audioconvert ! tee name=rawaudiotee
        #    rawvideotee. ! queue ! x264enc tune=zerolatency bitrate=800000 key-int-max=5 name=encvideosrc
        #    rawaudiotee. ! queue ! avenc_aac name=encaudiosrc
        #)''')
        pipeline = Gst.parse_launch('''(
            rtph264depay name=depay0 ! h264parse ! tee name=encvideosrc
            rtpmp4gdepay name=depay1 ! aacparse ! tee name=encaudiosrc
            encvideosrc. ! queue ! avdec_h264 ! queue ! videoconvert ! tee name=rawvideotee
            encaudiosrc. ! queue ! avdec_aac ! queue ! audioconvert ! tee name=rawaudiotee
        )''')
        pipeline.set_property('message-forward', True)

        ##
        ## This is copied from backlog.py, with tweaked variable names
        ##

        vq = Gst.ElementFactory.make('queue', 'prerecord-video-queue')
        aq = Gst.ElementFactory.make('queue', 'prerecord-audio-queue')
        mux = Gst.ElementFactory.make('mp4mux', 'prerecord-mp4-muxer')
        filesink = Gst.ElementFactory.make('filesink', 'prerecord-filesink')

        pipeline.add(vq)
        pipeline.add(aq)
        pipeline.add(mux)
        pipeline.add(filesink)

        vq.set_properties(
            max_size_time=20 * Gst.SECOND,
            max_size_bytes=0, # disable
            max_size_buffers=0, # disable
            leaky=2, # leaky on downstream, so toss old buffers
        )
        aq.set_properties(
            max_size_time=20 * Gst.SECOND,
            max_size_bytes=0, # disable
            max_size_buffers=0, # disable
            leaky=2, # leaky on downstream, so toss old buffers
        )
        filesink.set_property('async', False)

        # construct rest of pipeline
        mux.link(filesink)
        mux_video_sink = mux.get_request_pad('video_%u')
        mux_audio_sink = mux.get_request_pad('audio_%u')
        vq.link_pads('src', mux, mux_video_sink.get_name()) # vidq -> mux:video_%u
        aq.link_pads('src', mux, mux_audio_sink.get_name()) # audioq -> mux:audio_%u

        # link first stage pipeline with backlog pipeline
        encvideosrc = pipeline.get_by_name('encvideosrc')
        encvideosrc.link(vq)
        encaudiosrc = pipeline.get_by_name('encaudiosrc')
        encaudiosrc.link(aq)

        # only show video for now
        streamp = Gst.parse_launch('''
            videoscale name=start
            ! video/x-raw,height=640,width=480
            ! queue
            ! x264enc tune=zerolatency speed-preset=ultrafast byte-stream=true threads=1 intra-refresh=true key-int-max=15 bitrate=1000
            ! rtph264pay
            ! queue
            ! udpsink host=0.0.0.0 port=1234 async=false
        ''')
        pipeline.add(streamp)
        rawvideotee = pipeline.get_by_name('rawvideotee')
        rawvideotee.link(streamp.get_by_name('start'))


        # DEBUGGING: playback audio/video
        #videosink = Gst.ElementFactory.make('autovideosink', 'videosink')
        #audiosink = Gst.ElementFactory.make('autoaudiosink', 'audiosink')
        #pipeline.add(videosink)
        #pipeline.add(audiosink)
        #videosink.set_property('sync', True)
        #audiosink.set_property('sync', True)
        #rawvideotee = pipeline.get_by_name('rawvideotee')
        #rawaudiotee = pipeline.get_by_name('rawaudiotee')
        #rawvideotee.link(videosink)
        #rawaudiotee.link(audiosink)

        return pipeline

class RecordingMedia(GstRtspServer.RTSPMedia):
    def __init__(self):
        GstRtspServer.RTSPMedia.__init__(self)

        self.vq = None
        self.aq = None
        self.mux = None
        self.filesink = None

        self.vq_src = None
        self.aq_src = None
        self.vq_src_probe = None
        self.aq_src_probe = None
        self.vbuffer_count = 0
        self.abuffer_count = 0

        self.eos_count = 0
        self.started_recording = False
        self.is_video_recording_ready = False

    def initialize_backlog(self):
        el = self.get_element()
        assert el != None

        # el should be GstBin
        el.set_property('message-forward', True)

        self.vq = el.get_by_name('prerecord-video-queue')
        self.aq = el.get_by_name('prerecord-audio-queue')
        self.mux = el.get_by_name('prerecord-mp4-muxer')
        self.filesink = el.get_by_name('prerecord-filesink')

        # install blocking pads on both video and audio src pads
        self.vq_src = self.vq.get_static_pad('src')
        self.vq_src_probe = self.vq_src.add_probe(
            Gst.PadProbeType.BLOCK | Gst.PadProbeType.BUFFER,
            block_probe_cb,
            None,
            None,
        )

        self.aq_src = self.aq.get_static_pad('src')
        self.aq_src_probe = self.aq_src.add_probe(
            Gst.PadProbeType.BLOCK | Gst.PadProbeType.BUFFER,
            block_probe_cb,
            None,
            None,
        )

        # finally, update filesink location
        self.update_filesink_location()

        with open('pipeline.dot', 'w') as fp:
            fp.write(Gst.debug_bin_to_dot_data(el, Gst.DebugGraphDetails.ALL))

    def update_filesink_location(self):
        # TODO remove hardcoded path
        name = path.join('videos', time.strftime('%Y-%m-%dT%H-%M-%SZ%z.mp4'))
        self.filesink.set_property('location', name)

    def start_recording(self):
        if not self.started_recording:
            self.started_recording = True
            # this is updated in the probe callback below
            self.is_video_recording_ready = False

            self.vbuffer_count = 0
            self.abuffer_count = 0
            self.vq_src.add_probe(Gst.PadProbeType.BUFFER, self.probe_drop_one_cb, 'video', None)
            self.aq_src.add_probe(Gst.PadProbeType.BUFFER, self.probe_drop_one_cb, 'audio', None)

            self.vq_src.remove_probe(self.vq_src_probe)
            self.aq_src.remove_probe(self.aq_src_probe)
            self.vq_src_probe = None
            self.aq_src_probe = None

    def stop_recording(self):
        if self.started_recording:
            self.started_recording = False
            self.is_video_recording_ready = False
            self.vq_src_probe = self.vq_src.add_probe(
                Gst.PadProbeType.BLOCK | Gst.PadProbeType.BUFFER,
                block_probe_cb,
                None,
                None,
            )
            self.aq_src_probe = self.aq_src.add_probe(
                Gst.PadProbeType.BLOCK | Gst.PadProbeType.BUFFER,
                block_probe_cb,
                None,
                None,
            )

        # tell muxer to finish saving files
        # need to send EOS on both pads to properly finish writing both audio and video
        self.eos_count = 0
        GLib.idle_add(self.push_eos, self.vq_src)
        GLib.idle_add(self.push_eos, self.aq_src)

    def reset_recording(self):
        print('reset recording begin')
        # clean up muxer and filesink
        self.mux.set_state(Gst.State.NULL)
        self.filesink.set_state(Gst.State.NULL)

        # generate new save location
        self.update_filesink_location()

        # restart mux and filesink
        # restarting filesink will make it use new location
        self.mux.set_state(Gst.State.PLAYING)
        self.filesink.set_state(Gst.State.PLAYING)
        print('reset recording end')

    def push_eos(self, pad):
        peer = pad.get_peer()
        print('pushing eos to pad', peer.get_name())
        peer.send_event(Gst.Event.new_eos())
        return None

    def probe_drop_one_cb(self, pad, info, user_data, destroy_data):
        # what pad is this probe installed on (video or audio queue)
        pad_name = user_data
        buf = info.get_buffer()
        if pad_name == 'video':
            if self.vbuffer_count == 0:
                self.vbuffer_count += 1
                print(f'Dropping one video buffer with ts {Gst.TIME_ARGS(buf.pts)}')
                return Gst.PadProbeReturn.DROP
        elif pad_name == 'audio':
            if self.abuffer_count == 0:
                self.abuffer_count += 1
                print(f'Dropping one audio buffer with ts {Gst.TIME_ARGS(buf.pts)}')
                return Gst.PadProbeReturn.DROP

        if pad_name == 'video':
            is_keyframe = not buf.has_flags(Gst.BufferFlags.DELTA_UNIT)
            if is_keyframe:
                # buf is a keyframe, so let buffer through and remove this probe
                print('Keyframe at:', Gst.TIME_ARGS(buf.pts))
                # we now can actually record both video and audio, since
                # we have reached a video keyframe
                self.is_video_recording_ready = True
                return Gst.PadProbeReturn.REMOVE
            # wait for a keyframe
            return Gst.PadProbeReturn.DROP
        elif pad_name == 'audio':
            # wait for video to start recording
            if self.is_video_recording_ready:
                return Gst.PadProbeReturn.REMOVE
            return Gst.PadProbeReturn.DROP

    def do_handle_message(self, message):
        # can't use super() here. I have to explicitly reference the superclass.
        # https://developer.gnome.org/gobject/stable/howto-gobject-chainup.html
        # https://developer.gnome.org/gobject/stable/howto-gobject-methods.html
        GstRtspServer.RTSPMedia.do_handle_message(self, message)

        t = message.type
        if Gst.MessageType.ELEMENT == t:
            if message.has_name('GstBinForwarded'):
                # key-value metadata for message
                msg_struct = message.get_structure()
                # the forwarded message is in the structure under key "message"
                # see GstBin message-forward property for more info
                fwdmsg = msg_struct.get_value('message')
                t = fwdmsg.type

                if Gst.MessageType.EOS == t:
                    self.reset_recording()

                if Gst.MessageType.EOS == t:
                    if self.eos_count == 2:
                        self.eos_count = 0
                        self.reset_recording()
                    else:
                        self.eos_count += 1

        return True

class RtspServer(object):
    def __init__(self):
        # setup rtsp server
        self.rtsp_server = GstRtspServer.RTSPServer.new()
        self.rtsp_server.set_property('service', '1880')

        self.medias = []

        # mounts is a mapping from uris to media factories
        mounts = self.rtsp_server.get_mount_points()

        # construct a recording media factory
        #elf.rec_factory = GstRtspServer.RTSPMediaFactory.new()
        self.rec_factory = RecordingFactory()
        # This is how you get RecordingMedia's gtype. MediaFactory will use this gtype
        # in constructing a media: g_object_new(media_gtype, ...)
        # https://github.com/GStreamer/gst-rtsp-server/blob/master/gst/rtsp-server/rtsp-media-factory.c
        self.rec_factory.set_media_gtype(RecordingMedia.__gtype__)
        self.rec_factory.set_transport_mode(GstRtspServer.RTSPTransportMode.RECORD)
        self.rec_factory.set_eos_shutdown(True) # send EOS to media upon shutdown
        self.rec_factory.set_latency(10)
        #self.rec_factory.set_launch('( rtph264depay name=depay0 ! queue ! h264parse ! avdec_h264 ! videoconvert ! autovideosink sync=true rtpmp4gdepay name=depay1 ! queue ! avdec_aac ! audioconvert ! autoaudiosink sync=true )')

        self.rec_factory.connect('media-constructed', self.on_media_constructed)
        self.rec_factory.connect('media-configure', self.on_media_configure)

        # mount media factory to a uri
        mounts.add_factory('/phone', self.rec_factory)

        # attach server to default main context
        self.rtsp_server.attach(None)

    def on_media_constructed(self, media_factory, media):
        print('on media constructed')
        media.initialize_backlog()
        media.connect('unprepared', self.on_media_unprepared)
        self.medias.append(media)

    def on_media_configure(self, media_factory, media):
        print('on media configure')
        #self.rec_factory.prerecord.pbin.set_state(Gst.State.PLAYING)

        # DEBUGGING start recording automatically
        #GLib.timeout_add_seconds(5, self.start_recording)

    def on_media_unprepared(self, media):
        print('on media unprepared')
        self.medias.remove(media)

    def start_recording(self):
        print('start recording')
        for media in self.medias:
            media.start_recording()

        # DEBUGGING stop recording after 5 seconds
        #GLib.timeout_add_seconds(5, self.stop_recording)
        return False

    def stop_recording(self):
        print('stop recording')
        for media in self.medias:
            media.stop_recording()
        return False

def on_mqtt_message(client, userdata, msg):
    if msg.topic == STREAM_SERVER_CONTROL_TOPIC:
        state = userdata['state']
        server = userdata['server']
        cmd = msg.payload.decode('ascii')
        print('got control command', cmd, 'with current rec state', state.recording)
        if cmd == 'START_RECORD' and not state.recording:
            server.start_recording()
            state.recording = True
        elif cmd == 'STOP_RECORD' and state.recording:
            server.stop_recording()
            state.recording = False

        # publish state even if there's an invalid message, since
        # I just want something to work and don't care about consistent
        # API surface
        # also I don't wait for this b/c it's state and I don't care if
        # it doesn't actually get published. Probably a bad decision,
        # but maybe I'll detect publish failure later.
        client.publish(STREAM_SERVER_STATE_TOPIC, repr(state))

    elif msg.topic == PHONECTRL_PHOTO_TOPIC:
        print('got photo. saving.')
        name = path.join('photos', time.strftime('%Y-%m-%dT%H-%M-%SZ%z.jpg'))
        with open(name, 'wb') as fp:
            fp.write(msg.payload)

class State(object):
    recording = False
    def __repr__(self):
        return f'recording:{self.recording}'

if __name__ == '__main__':
    broker_server = os.environ.get('MQTT_BROKER')
    if broker_server is None:
        print('Need MQTT_BROKER env')
        sys.exit(1)
    if ':' not in broker_server:
        print('Need host and port')
        sys.exit(1)

    MQTT_HOST, MQTT_PORT = broker_server.rsplit(':')
    MQTT_PORT = int(MQTT_PORT)

    Gst.init(None)

    server = RtspServer()
    loop = GLib.MainLoop()

    state = State()

    userdata = {
        'state': state,
        'server': server,
    }
    mqttc = mqtt.Client(client_id='streamsrv' + str(random.random()), userdata=userdata)
    mqttc.on_message = on_mqtt_message

    try:
        print('starting')
        mqttc.connect(MQTT_HOST, MQTT_PORT, 60)
        mqttc.loop_start()
        mqttc.subscribe(STREAM_SERVER_CONTROL_TOPIC)
        mqttc.subscribe(PHONECTRL_PHOTO_TOPIC)

        print('running')
        loop.run()
    except KeyboardInterrupt:
        pass
    finally:
        mqttc.disconnect()
