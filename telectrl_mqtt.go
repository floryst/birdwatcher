package main

import (
	"log"
	"math/rand"
	"os"
	"strconv"
	"time"

	utils "./telectrlutils"
	mqtt "github.com/eclipse/paho.mqtt.golang"
)

// ControlTopic control topic
const ControlTopic = "telectrl/control"

// StateTopic state topic
const StateTopic = "telectrl/state"

// Direction direction
type Direction int

// Action action
type Action int

const (
	actionMove Action = iota
	actionStop
	actionStopAll
	actionSlewRate
)

const (
	dirUp Direction = iota
	dirDown
	dirLeft
	dirRight
)

type telescopeAction struct {
	kind Action
	dir  Direction
	rate byte
}

type telescopeState2 struct {
	actionQueue []telescopeAction
	moving      [4]bool
	slewRate    byte
}

func commandFromAction(act telescopeAction) []byte {
	moveDirs := []byte{'n', 's', 'w', 'e'}
	if act.kind == actionMove {
		return []byte{':', 'M', moveDirs[act.dir], '#'}
	} else if act.kind == actionStop {
		return []byte{':', 'Q', moveDirs[act.dir], '#'}
	} else if act.kind == actionStopAll {
		return []byte{':', 'Q', '#'}
	} else if act.kind == actionSlewRate {
		return []byte{':', 'S', 'w', act.rate + '0', '#'}
	}
	return []byte{}
}

func (ts *telescopeState2) MoveDir(dir Direction) {
	if !ts.moving[dir] {
		ts.moving[dir] = true
		ts.actionQueue = append(ts.actionQueue, telescopeAction{kind: actionMove, dir: dir})
	}
}

func (ts *telescopeState2) StopDir(dir Direction) {
	if ts.moving[dir] {
		ts.moving[dir] = false
		ts.actionQueue = append(ts.actionQueue, telescopeAction{kind: actionStop, dir: dir})
	}
}

func (ts *telescopeState2) StopAll() {
	ts.actionQueue = append(ts.actionQueue, telescopeAction{kind: actionStopAll})
}

func (ts *telescopeState2) SlewRate(rate byte) {
	if rate > 1 && rate < 8 && rate != ts.slewRate {
		ts.actionQueue = append(ts.actionQueue, telescopeAction{kind: actionSlewRate, rate: rate})
	}
}

func initMqttClient() (mqtt.Client, chan mqtt.Message) {
	// for debugging
	// mqtt.DEBUG = log.New(os.Stdout, "", 0)
	mqtt.ERROR = log.New(os.Stderr, "", 0)
	broker, found := os.LookupEnv("MQTT_BROKER")
	if !found {
		broker = "tcp://127.0.0.1:1883"
	}
	opts := mqtt.NewClientOptions()
	opts.AddBroker(broker)
	opts.SetClientID("mqtt.telectrl-" + strconv.Itoa(rand.Int()))
	opts.SetKeepAlive(2 * time.Second)
	opts.SetPingTimeout(1 * time.Second)

	publish := make(chan mqtt.Message)
	opts.SetDefaultPublishHandler(func(client mqtt.Client, msg mqtt.Message) {
		publish <- msg
	})

	return mqtt.NewClient(opts), publish
}

func main() {
	var err error

	serialTTY, found := os.LookupEnv("SERIAL_TTY")
	if !found {
		log.Fatal("Cannot find SERIAL_TTY env")
	}

	port, err := utils.OpenSerialPort(serialTTY)
	if err != nil {
		log.Fatal(err)
	}
	defer port.Close()

	defer func() {
		log.Println("Stopping any telescope movement")
		time.Sleep(10 * time.Millisecond)
		msg := []byte{':', 'Q', '#'}
		if _, err := port.Write(msg); err != nil {
			log.Fatal(err)
		}
	}()

	mqttClient, pubChannel := initMqttClient()
	defer mqttClient.Disconnect(300)

	if token := mqttClient.Connect(); token.Wait() && token.Error() != nil {
		log.Fatal("Failed to connect:", token.Error())
	}

	if token := mqttClient.Subscribe(ControlTopic, 0, nil); token.Wait() && token.Error() != nil {
		log.Fatal("Failed to subscribe to control topic:", token.Error())
	}

	teleState := telescopeState2{
		actionQueue: make([]telescopeAction, 0),
		slewRate:    4,
	}
	log.Println("Listening for messages...")
	for {
		msg := <-pubChannel
		log.Println("got message with topic:", msg.Topic())
		log.Println("...with payload:", msg.Payload())
		if msg.Topic() == ControlTopic {
			payload := string(msg.Payload())
			switch payload {
			case "UP":
				teleState.MoveDir(dirUp)
			case "DOWN":
				teleState.MoveDir(dirDown)
			case "LEFT":
				teleState.MoveDir(dirLeft)
			case "RIGHT":
				teleState.MoveDir(dirRight)
			case "STOP_UP":
				teleState.StopDir(dirUp)
			case "STOP_DOWN":
				teleState.StopDir(dirDown)
			case "STOP_LEFT":
				teleState.StopDir(dirLeft)
			case "STOP_RIGHT":
				teleState.StopDir(dirRight)
			case "STOP_ALL":
				teleState.StopAll()
			case "SLEW_RATE_2":
				teleState.SlewRate(2)
			case "SLEW_RATE_3":
				teleState.SlewRate(3)
			case "SLEW_RATE_4":
				teleState.SlewRate(4)
			}

			// I may support action queue collapsing later, but not now
			if len(teleState.actionQueue) == 1 {
				action := teleState.actionQueue[0]
				teleState.actionQueue = teleState.actionQueue[1:]

				cmd := commandFromAction(action)
				log.Println("Executing command", string(cmd))
				if _, err := port.Write(cmd); err != nil {
					log.Println("Failed to write command to telescope", err)
				} else {
					time.Sleep(100 * time.Millisecond)
					if action.kind == actionSlewRate {
						resp, err := port.ReadTeleResponse(utils.ZeroOneTermFunc)
						if err != nil {
							log.Println("Failed to read slew set response", err)
						} else if resp[0] != '1' {
							log.Println("Failed to set slew rate")
						}
					}
				}

				// TODO
				// mqttClient.Publish(StateTopic, 0, false, teleState)
			}
		}
	}
}
