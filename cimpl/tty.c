#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>

int main() {
  int port =  open("/dev/ttyUSB0", O_RDWR);
  if (port < 0) {
    printf("error %i from open: %s\n", errno, strerror(errno));
    exit(1);
  }

  struct termios tty;
  memset(&tty, 0, sizeof tty);

  if (tcgetattr(port, &tty) != 0) {
    printf("error %i from tcgetattr: %s\n", errno, strerror(errno));
    exit(1);
  }

  printf("default cflag: %d\n", tty.c_cflag);

  tty.c_cflag &= ~PARENB; // clear parity bit
  tty.c_cflag &= ~CSTOPB; // use 1 stop bit
  tty.c_cflag |= CS8; // 8 data bits
  tty.c_cflag &= ~CRTSCTS; // disable RTS/CTS flow control
  tty.c_cflag |= CREAD | CLOCAL; // turn on read and ignore ctrl lines

  tty.c_lflag &= ~ICANON; // disable canonical mode
  tty.c_lflag &= ~(ECHO | ECHOE | ECHONL);
  tty.c_lflag &= ~ISIG;

  tty.c_iflag &= ~(IXON | IXOFF | IXANY);
  tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL);

  tty.c_oflag &= ~OPOST;
  tty.c_oflag &= ~ONLCR;

  tty.c_cc[VTIME] = 50; // deciseconds
  tty.c_cc[VMIN] = 0;

  cfsetispeed(&tty, B9600);
  cfsetospeed(&tty, B9600);

  if (tcsetattr(port, TCSANOW, &tty) != 0) {
    printf("error %i from tcsetattr: %s\n", errno, strerror(errno));
    exit(1);
  }

  unsigned char msg[] = { ':', 'G', 'C', '#' };
  write(port, msg, sizeof(msg));

  char readbuf[256];
  memset(&readbuf, '\0', sizeof(readbuf));

  int n = read(port, &readbuf, sizeof(readbuf));
  printf("Read: %d bytes\n", n);
  printf("Read buffer: '%s'\n", readbuf);

  close(port);

  return 0;
}
