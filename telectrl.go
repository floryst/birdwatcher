package main

import (
	"errors"
	"log"
	"net"
	"os"
	"os/signal"
	"time"

	"syscall"

	utils "./telectrlutils"
)

type telescopeState struct {
	activeDirs [4]bool
	slewRate   byte
}

func max(a byte, b byte) byte {
	if a >= b {
		return a
	}
	return b
}

func min(a byte, b byte) byte {
	if a <= b {
		return a
	}
	return b
}

func handleMsg(port *utils.Port, msg []byte, state *telescopeState) error {
	movedirs := []byte{'n', 'e', 's', 'w'}
	cmdByte := msg[0]

	var teleCmd []byte
	action := (cmdByte >> 6) & 0x3
	switch action {
	case 0:
		// set slew rate
		slewRate := max(2, min(8, cmdByte&0xf))
		if state.slewRate == slewRate {
			return nil
		}
		teleCmd = []byte{':', 'S', 'w', slewRate + '0', '#'}
		state.slewRate = slewRate
		break
	case 1:
		// start movement
		dir := cmdByte & 0x3
		dirChar := movedirs[dir]
		if !state.activeDirs[dir] {
			teleCmd = []byte{':', 'M', dirChar, '#'}
			state.activeDirs[dir] = true
		}
		break
	case 2:
		// stop movement
		dir := cmdByte & 0x3
		dirChar := movedirs[dir]
		if state.activeDirs[dir] {
			teleCmd = []byte{':', 'Q', dirChar, '#'}
			state.activeDirs[dir] = false
		}
		break
	case 3:
		// stop all slewing
		teleCmd = []byte{':', 'Q', '#'}
		state.activeDirs = [4]bool{false, false, false, false}
		break
	default:
		return nil
	}

	if _, err := port.Write(teleCmd); err != nil {
		return err
	}
	log.Println("Write command:", string(teleCmd))
	time.Sleep(100 * time.Millisecond)

	if action == 0 {
		// slew rate response
		resp, err := port.ReadTeleResponse(utils.ZeroOneTermFunc)
		if err != nil {
			return err
		} else if resp[0] != '1' {
			return errors.New("Failed to set slew rate")
		}
	}
	return nil
}

func main() {
	var err error
	// port, err := OpenSerialPort("/dev/ttyUSB0")
	port, err := utils.OpenSerialPort("/dev/pts/8")
	if err != nil {
		log.Fatal(err)
	}
	defer port.Close()

	serviceServer := utils.MakeDiscoverableServer("service-telectrl", 8353)
	serviceServer.Start()
	defer serviceServer.Stop()

	// use UDP since I don't care about clients. I only
	// care about commands. Plus, I want individual packets to
	// indicate control, and tcp congestion may squeeze several
	// 1-byte commands into the same packet.
	conn, err := net.ListenPacket("udp", ":8353")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	// quit all movement if server shuts down
	defer func() {
		log.Println("Stopping any telescope movement")
		time.Sleep(10 * time.Millisecond)
		msg := []byte{':', 'Q', '#'}
		if _, err := port.Write(msg); err != nil {
			log.Fatal(err)
		}
	}()

	go func() {
		recv := make([]byte, 1)
		state := telescopeState{slewRate: 8} // I think telescope starts out at max slew rate
		for {
			if conn == nil {
				return
			}
			n, _, err := conn.ReadFrom(recv)
			if err != nil {
				log.Println("packetConn err:", err)
				return
			} else if n == 0 {
				log.Println("Empty packet!")
			} else if err := handleMsg(port, recv, &state); err != nil {
				log.Println(err)
			}
		}
	}()

	/*
		var msg []byte
		var resp []byte

		msg = []byte{':', 'G', 'C', '#'}
		if _, err := port.Write(msg); err != nil {
			log.Fatal(err)
		}

		resp, err = port.ReadTeleResponse(HashTermFunc)
		if err != nil {
			log.Fatal(err)
		}
		log.Println(string(resp))
	*/

	intChan := make(chan os.Signal, 1)
	signal.Notify(intChan, os.Interrupt, syscall.SIGTERM)
	<-intChan
}
