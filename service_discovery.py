import socket
import select
import time
from socket import socket, AF_INET, SOCK_DGRAM, SOL_SOCKET, SO_BROADCAST, gethostbyname, gethostname

s = socket(AF_INET, SOCK_DGRAM)
s.bind(('', 0))
s.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
s.setblocking(0)

while True:
    print('Searching...')
    data = b'SRCH:service-telectrl'
    s.sendto(data, ('<broadcast>', 4111))

    ready, _, _ = select.select([s], [], [], 5)
    if ready:
        data, addr = s.recvfrom(512)
        print(data, addr)
        break
